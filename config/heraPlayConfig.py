# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module heraPlayConfig
#   FN20170529 creation
#   FN20170612 wsmaxretry, hidefield, wssearchperiod, tenantid
#   FN20170615 pagesize
#   FN20170621 wsauditpwd, cpFilter
#   FN20170622 get mapping from field
#   FN20170712 hidetime
#   FN20170828 wsmaxresults removed
#   FN20170831 lamebin and exportpath added
#   FN20170904 innerlimit
#

import logging.config
import configparser
from os import path


class Config:

    def __init__(self):
        logpath = path.join(path.dirname(path.abspath(__file__)), 'heraPlay.log.conf')
        logging.config.fileConfig(logpath)
        self.log = logging.getLogger('asclinkreplay')
        log = self.log

        config_reader = configparser.ConfigParser()
        cfgpath = path.join(path.dirname(path.abspath(__file__)), 'heraPlay.conf')
        config_reader.read(cfgpath)
        # section heraplay
        self.port = int(config_reader['heraplay']['port'])
        log.info('port: %s' % self.port)
        self.root_directory = config_reader['heraplay']['root_directory']
        log.info('root_directory: %s' % self.root_directory)
        self.wsurl = config_reader['heraplay']['wsurl']
        log.info('wsurl: %s' % self.wsurl)
        self.lamebin = config_reader['heraplay']['lamebin']
        log.info('lamebin: %s' % self.lamebin)
        # section mapping
        self.connId = config_reader['mapping']['conn_id']
        log.info('connId: %s' % self.connId)
        self.place = config_reader['mapping']['place']
        log.info('place: %s' % self.place)
        self.dnis = config_reader['mapping']['dnis']
        log.info('dnis: %s' % self.dnis)
        self.servizio = config_reader['mapping']['servizio']
        log.info('servizio: %s' % self.servizio)
        self.struttura = config_reader['mapping']['struttura']
        log.info('struttura: %s' % self.struttura)
        self.esigenza = config_reader['mapping']['esigenza']
        log.info('esigenza: %s' % self.esigenza)
        self.motivo_registrazione = config_reader['mapping']['motivo_registrazione']
        log.info('motivo_registrazione: %s' % self.motivo_registrazione)
        # section certificate
        self.cert_perm = config_reader['certificate']['cert_pem']
        self.cert_key = config_reader['certificate']['cert_key']
        # section linkReplay
        self.start_year = config_reader['linkReplay']['start_year']
        log.info('start year %s' % self.start_year)
        self.stop_year = config_reader['linkReplay']['stop_year']
        log.info('stop year %s' % self.stop_year)
        # Equal to 0 means we are in dev, 1 in production
        self.dev_level = config_reader.getboolean('environment', 'dev_level')
        log.info('Dev Level: %s' % self.dev_level)
        self.delete_min_time = config_reader['environment']['delete_min_time']
        log.info('Delete time: %s' % self.delete_min_time)

