# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Package config
#   FN20170529 creation
#

from .heraPlayConfig import Config

cfg = Config()
log = cfg.log
log.info('HeraPlay v0.25.2 - Reading configuration...')
