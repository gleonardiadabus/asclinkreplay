# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module AuditForm
#   FN20170612 creation
#   FN20170615 validation, fixing field type from type to input (using jquery datepicker on all browsers)
#   FN20170623 adding titles, default dates as today
#   FN20170629 moving default init to instance creation
#

from wtforms import Form, DateField, validators


class AuditForm(Form):
    fromdate = DateField(u'Data da', [validators.DataRequired('Il campo "Data da" non può essere vuoto')],
                         render_kw={'placeholder': 'Data da', 'class': 'input', 'type': 'input', 'title':u'Data da'},
                         format='%Y-%m-%d')
    todate = DateField(u'Data a', [validators.DataRequired('Il campo "Data a" non può essere vuoto')],
                       render_kw={'placeholder': 'Data a', 'class': 'input', 'type': 'input', 'title':u'Data a'},
                       format='%Y-%m-%d')
