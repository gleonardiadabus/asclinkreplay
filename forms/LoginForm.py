# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module LoginForm
#   FN20170526 creation
#   FN20170612 styling
#   FN20170615 validation
#   FN20170623 adding titles
#   FN20170904 error description
#

from wtforms import Form, StringField, PasswordField, validators


class LoginForm(Form):
    username = StringField(u'Utente', [validators.DataRequired('Il campo "Utente" non può essere vuoto')],
                           render_kw={'placeholder': 'Utente', 'class': 'input', 'title':u'Utente'})
    password = PasswordField(u'Password', [validators.DataRequired('Il campo "Password" non può essere vuoto')],
                             render_kw={'placeholder': 'Password', 'class': 'input', 'title':u'Password'})
