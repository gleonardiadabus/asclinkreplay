# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module SearchForm
#   FN20170523 creation
#   FN20170608 select field and css injected classes
#   FN20170616 init form values
#   FN20170623 adding titles
#   FN20170711 adding durations
#   FN20170718 select fields
#   FN20170901 max duration set to 02:00:00
#   FN20170904 error description
#

from wtforms import Form, StringField, SelectField, validators

# regex = r'^((([01]?[0-9]|2[0-3]):)?[0-5][0-9]:[0-5][0-9])?$'
regex = r'^((0[0-2]:)?[0-5][0-9]:[0-5][0-9])?$'


class SearchForm(Form):
    servizio = SelectField(label=u'Servizio',
                           render_kw={'placeholder':u'Servizio', 'class':'select', 'title':u'Servizio'})
    esigenza = SelectField(label=u'Esigenza',
                           render_kw={'placeholder':u'Esigenza', 'class':'select', 'title':u'Esigenza'})
    motivo_registrazione = SelectField(label=u'Motivo registrazione',
                           render_kw={'placeholder':u'Motivo registrazione', 'class':'select', 'title':u'Motivo registrazione'})
    telefono = StringField(label=u'Telefono cliente',
                           render_kw={'placeholder':u'Telefono Cliente', 'class':'input', 'title':u'Telefono Cliente'})
    outsourcer = SelectField(label=u'Outsourcer',
                             render_kw={'placeholder': u'Outsourcer', 'class':'select', 'title':u'Outsourcer'})
    direzione = SelectField(label=u'Direzione',
                            render_kw={'placeholder':u'Direzione', 'class':'select', 'title':u'Direzione'})
    duratamin = StringField(label=u'Durata Minima',
                            validators=[validators.regexp(regex, message='Formato del campo "Durata Minima" errato. Formati validi: MM:SS e HH:MM:SS. '
                                                                                                 'Valore massimo accettato: 02:00:00')],
                            render_kw={'placeholder':u'Durata Minima', 'class':'input', 'title':u'Durata Minima'})
    duratamax = StringField(label=u'Durata Massima',
                            validators=[validators.regexp(regex, message='Formato del campo "Durata Massima" errato. Formati validi: MM:SS e HH:MM:SS. '
                                                                                                  'Valore massimo accettato: 02:00:00')],
                            render_kw={'placeholder':u'Durata Massima', 'class':'input', 'title':u'Durata Massima'})