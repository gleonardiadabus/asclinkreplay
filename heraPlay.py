import os
from flask import Flask, flash, render_template, request, redirect, url_for, session, send_file
from flask_session import Session
from common.customserializer import CustomSerializer
from common.replayConversation import ReplayConversation
from config import cfg, log
from forms.LoginForm import LoginForm
from utils_function import login_required, flash_error
import subprocess
import requests
from common.xmlRequest import XmlRequest
import xml.etree.ElementTree as ET
import urllib
import atexit

from apscheduler.schedulers.background import BackgroundScheduler


def program_delete():
    folder=os.path.join(cfg.root_directory,'static','mp3')
    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            log.error("An error occured deleting file:",e)
    log.debug("System programmatically delete at %I:%M:%S")


scheduler = BackgroundScheduler()
scheduler.add_job(func=program_delete, trigger="interval", minutes=int(float(cfg.delete_min_time)))
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: scheduler.shutdown())





app = Flask(__name__)


# session configuration
SESSION_TYPE = 'filesystem'
SESSION_FILE_THRESHOLD = 10000
PERMANENT_SESSION_LIFETIME = 36000  # 10 hour
app.config.from_object(__name__)
Session(app)

# custom JSON encoder/decoder
app.json_encoder = CustomSerializer

# SSL certificates
# certificate support for https
ssl_certificates = (cfg.cert_perm, cfg.cert_key)

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route('/search')
@login_required
def search():
    connId = request.args.get('connId')
    callId = request.args.get('callId')

    if not connId:
        flash_error("ConnId non valida")
        return render_template('linkReplay.html',
                               user=session['user'],
                               results={})

    if not callId:
        flash_error("CallId non valida")
        return render_template('linkReplay.html',
                               user=session['user'],
                               results={})

    s = requests.session()
    if 'jsessionid' not in session:
        xml = XmlRequest.loginRequest.replace("{USERNAME}", session['user']).replace("{PASSWORD}", session['pwd'])
        headers = {'Content-Type': 'application/xml'}
        r = s.post(cfg.wsurl, data=xml, headers=headers, verify=False)
        tree = ET.fromstring(r.content)
        jsessionid = tree.attrib['target']
        session['jsessionid']=jsessionid[7:]
    log.debug("trying to retrieve conversation from connid...")
    xml = XmlRequest.searchRequest.replace("{CONNID_VALUE}", connId)
    headers = {'Content-Type': 'application/xml'}
    url = cfg.wsurl+session['jsessionid']
    r = s.post(url, data=xml, headers=headers, verify=False)
    tree = ET.fromstring(r.content)
    conversations = []
    # Sessione invalidata
    if tree.attrib['error']=='3':
        flash_error("Sessione scaduta!")
        logout()
        session['connId']=connId
        session['callId']=callId
        return redirect(url_for('login'))

    for child in tree:
        if child.tag=='call' and child[0].tag=='id' and child[0].text == callId:
            log.debug("Call founded! Retrieve data...")
            conv = ReplayConversation(callId,connId)
            for callChild in child:
                if callChild.tag=='start-time':
                    conv.starttime=callChild.attrib['value']
                if callChild.tag=='end-time':
                    conv.endtime=callChild.attrib['value']
                if callChild.tag=='duration':
                    conv.durata=callChild.text
                if callChild.tag=='agent':
                    conv.agentId=callChild.text
                if callChild.tag=='phone-intern':
                    conv.thisDn=callChild.text
                if callChild.tag=='phone-extern':
                    conv.dnEsterno=callChild.text
                if callChild.tag=='agent':
                    conv.agentId=callChild.text
                if callChild.tag=='phone-intern':
                    conv.thisDN=callChild.text
                if callChild.tag=='fields':
                    for field in callChild:
                        if field.attrib['name'] == cfg.connId:
                            conv.connId=field.text
                        if field.attrib['name'] == cfg.place:
                            conv.place=field.text
                        if field.attrib['name'] == cfg.dnis:
                            conv.dnis=field.text
                        if field.attrib['name'] == cfg.servizio:
                            conv.servizio=field.text
                        if field.attrib['name'] == cfg.struttura:
                            conv.struttura=field.text
                        if field.attrib['name'] == cfg.esigenza:
                            conv.esigenza=field.text
                        if field.attrib['name'] == cfg.motivo_registrazione:
                            conv.motivo_registrazione=field.text
            prepareMp3(conv,s)
            if not conv.url:
                flash_error("No wave for connId " + connId+" and callId "+callId)
            else:
                conv.url = convert_mp3(conv.url,conv.callId,s)
            logout(clear_session=False, socket=s)
            conversations.append(conv)
            s.close()
    if not len(conversations):
        flash_error("No conversations for connId " + connId+" and callId "+callId)
    return render_template('linkReplay.html',
                           user=session['user'],
                           results=conversations)


@app.route('/login', methods=['GET', 'POST'])
def login():
    # template selection
    template = 'login.html'
    s = None
    form = LoginForm(request.form)
    # already logged in
    if 'user' in session:
        return redirect(url_for('search'))
    # POST
    if request.method == 'POST':
        if form.validate():
            user = form.username.data
            pwd = form.password.data
            log.debug('Login attempt for user %s' % user)
            # login to neo
            try:
                xml = XmlRequest.loginRequest.replace("{USERNAME}", user).replace("{PASSWORD}", pwd)
                headers = {'Content-Type': 'application/xml'}
                r = requests.post(cfg.wsurl, data=xml, headers=headers, verify=False)
                tree = ET.fromstring(r.content)
                if tree.attrib['error'] == '0':
                    jsessionid = tree.attrib['target']
                    log.debug('User logged! Jsessionid:'+jsessionid)
                else:
                    flash_error("Invalida user name o password")
                    return redirect(url_for('login'))
            except Exception as e:
                flash_error("Login fallita per l'utente %s - neo WS exception: %s" % (user, e))
                return redirect(url_for('login'))

            # if res:
            if jsessionid:
                log.debug('Successful login for user %s' % user)
                # everything is ok, save session data:
                session['user'] = user
                session['pwd'] = pwd
                session['jsessionid'] = jsessionid[7:]
                connId = None
                callId = None
                if session.get('connId') and session.get('callId'):
                    connId = session['connId']
                    session.pop('connId', None)
                    callId = session['callId']
                    session.pop('callId', None)
                return redirect(url_for('search', connId=connId, callId=callId))
            else:
                flash_error("Login fallita per l'utente %s" % user)
                return redirect(url_for('login'))
        else:  # validation errors
            for error in form.username.errors + form.password.errors:
                flash('Errore di validazione: %s' % error)
            return render_template(template, form=form)
    else:
        # GET
        return render_template(template, form=form)

@app.route('/logout')
def logout(clear_session = True,socket=None):
    if 'jsessionid' in session:
        xml = XmlRequest.logoutRequest
        url = cfg.wsurl+session['jsessionid']
        headers = {'Content-Type': 'application/xml'}
        if not socket:
            r = requests.post(url, data=xml, headers=headers, verify=False)
        else:
            r = socket.post(url, data=xml, headers=headers, verify=False)
    if clear_session:
        session.clear()
    else:
        session.pop('jsessionid', None)
    return redirect(url_for('login'))


def prepareMp3(conv,s):
    xml = XmlRequest.mp3Request.replace("{CALLID_VALUE}",conv.callId)
    headers = {'Content-Type': 'application/xml'}
    url = cfg.wsurl+session['jsessionid']
    r = s.post(url, data=xml, headers=headers, verify=False)
    tree = ET.fromstring(r.content)
    for child in tree:
        if child.tag=='call':
            log.debug("Call founded! Retrieve mp3 link...")
            for callChild in child:
                if(callChild.tag=='link'):
                    conv.url=callChild.text


# def convert_mp3(source,callid):
#     urllib.request.urlretrieve(source, os.path.join('static', 'mp3', callid + '.wav'))
#     return 'mp3/'+callid + '.wav'

def convert_mp3(source, callid,s):
    log.debug("This is the source:"+ source)
    project_root = cfg.root_directory
    sourcePath = os.path.join(project_root, 'static', 'wav', callid + '.wav')
    urllib.request.urlretrieve(source, sourcePath)
    destination = os.path.join(project_root, 'static', 'mp3', callid + '.mp3')
    cmd = cfg.lamebin + ' --vbr-new ' + sourcePath + ' ' + destination
    if subprocess.call(cmd, shell=True) == 0:
        os.remove(sourcePath)
        # url = request.url_root + 'mp3/' + callid + '.mp3'
        url = 'mp3/'+callid+'.mp3'
        return url
    else:
        # in case of error return None as url
        if os.path.isfile(sourcePath):
            os.remove(sourcePath)
        return None

if __name__ == "__main__":
    # run the application
    app.run(host='0.0.0.0', ssl_context=ssl_certificates, port=cfg.port, threaded=True, debug=cfg.dev_level)

