#!/bin/bash
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module deploy
# Usage: deploy.sh <version>
#   FN20170629 creation
#   FN20170831 update (mp3)
#   FN20170904 update (certificates)
#   FN20170908 adding logman
#

if [ $# -ne 1 ]; then
    echo "Wrong parameters. Usage: deploy.sh <version>"
    exit
fi

VERSION=$1
SOURCEPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DEPLOYPATH="$SOURCEPATH/deploy/heraplay_$VERSION"

echo "Create deploy package for Heraplay $1"
echo "Path: $DEPLOYPATH ..."
mkdir -p $DEPLOYPATH
echo "Copying cert ..."
mkdir $DEPLOYPATH/cert
cp $SOURCEPATH/cert/certificate_cccim-recorder.hdc.local.key $DEPLOYPATH/cert/certificate.key
cp $SOURCEPATH/cert/certificate_cccim-recorder.hdc.local.pem $DEPLOYPATH/cert/certificate.pem
echo "Copying common ..."
cp -r $SOURCEPATH/common $DEPLOYPATH/common
echo "Copying config ..."
cp -r $SOURCEPATH/config $DEPLOYPATH/config
echo "Copying db ..."
cp -r $SOURCEPATH/db $DEPLOYPATH/db
echo "Copying forms ..."
cp -r $SOURCEPATH/forms $DEPLOYPATH/forms
echo "Creating log ..."
mkdir $DEPLOYPATH/log
echo "Creating mp3 ..."
mkdir $DEPLOYPATH/mp3
echo "Copying static ..."
cp -r $SOURCEPATH/static $DEPLOYPATH/static
echo "Copying templates ..."
cp -r $SOURCEPATH/templates $DEPLOYPATH/templates
echo "Copying ws ..."
cp -r $SOURCEPATH/ws $DEPLOYPATH/ws
echo "Copying root ..."
cp $SOURCEPATH/heraPlay.py $DEPLOYPATH
cp $SOURCEPATH/logman.bat $DEPLOYPATH
cp $SOURCEPATH/nssm.exe $DEPLOYPATH
cp $SOURCEPATH/requirements.txt $DEPLOYPATH
echo "Creating archive ..."
cd $SOURCEPATH/deploy/
TIMESTAMP=$(date +"%Y%m%d_%H%M%S")
ARCHIVENAME="heraplay_$VERSION.tar.gz"
tar czf $ARCHIVENAME heraplay_$VERSION
#mv $ARCHIVENAME "$ARCHIVENAME_$TIMESTAMP"
rm -r heraplay_$VERSION
cd - > /dev/null
echo "Done! Deploy package: $ARCHIVENAME"
