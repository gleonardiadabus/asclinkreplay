from config import cfg, log
from calendar import timegm
from time import strptime
import os
import subprocess
from flask import request


class NeoWSClient:
    def __init__(self, url):
        # self._client = Client(url)
        log.debug('neoWSClient initialized at %s' % url)

    def login(self, user, pwd):
        # res = helpers.serialize_object(self._client.service.testWebService(username=user, password=pwd))
        # log.debug('res: %s' % res)
        # if res['resultCode'] == 'PERMISSION_NO_SUPERUSER':
        #     return True
        # else:
        #     log.error('Login error with code: %s' % res['resultCode'])
        #     return False
        return True


    @staticmethod
    def __duration(duration):
        if len(duration) == 5:  # format MM:SS
            duration = '00:' + duration
        return timegm(strptime('1970-01-01 ' + duration, '%Y-%m-%d %H:%M:%S')) * 1000

    def get_link(self, user, pwd, callid, is_link):
        # export_state = ''
        # retry = 0
        # while (export_state != 'FINISHED') and (retry <= cfg.wsmaxretry):
        #     res = self._client.service.exportConversation(username=user, password=pwd, conversationId=callid)
        #     export_state = res.exportState
        #     if export_state == 'FINISHED':
        #         url = res.downloadURL
        #         if is_link:
        #             return url.replace('localhost', cfg.dbhost).replace('http', 'https')
        #         else:
        #             path = url.replace('http://localhost/ASCWebService/ConversationDownload', cfg.exportpath) + '.wav'
        #             if os.name == 'nt':
        #                 return path.replace('/', '\\')
        #             else:
        #                 return path
        #     else:
        #         sleep(5)
        #         retry += 1
        # # search timeout

        return 'timeout'

    @staticmethod
    def convert_mp3(source, callid):
        destination = os.path.join(os.getcwd(), 'mp3', callid + '.mp3')
        cmd = cfg.lamebin + ' --vbr-new ' + source + ' ' + destination
        if subprocess.call(cmd, shell=True) == 0:
            url = request.url_root + 'mp3/' + callid + '.mp3'
            return url
        else:
            # in case of error return None as url
            return None

    @staticmethod
    def check_callid(callid):
        if os.path.isfile(os.path.join(os.getcwd(), 'mp3', callid + '.mp3')):
            return request.url_root + 'mp3/' + callid + '.mp3'
        else:
            return None
