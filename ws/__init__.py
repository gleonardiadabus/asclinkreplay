# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Package ws
#   FN20170529 creation
#

from config import cfg
from .neoWSClient import NeoWSClient

url = cfg.wsurl
ws = NeoWSClient(url)
