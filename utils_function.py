# Utility functions used in heraPlay.py file
from flask import flash, redirect, url_for, session, request
from functools import wraps
from config import cfg, log


# Login required decorator: it checks if user is already logged in. If not, it redirects to the login page
def login_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        session['url'] = request.url
        log.debug(session)
        if 'user' in session:
            return fn(*args, **kwargs)
        else:
            if "connId"in session['url']:
                session['connId'] = request.args.get("connId")
                log.debug('connId: '+session['connId'])
            if "callId"in session['url']:
                session['callId'] = request.args.get("callId")
                log.debug('callId: '+session['callId'])
            log.debug('User not in session')
            return redirect(url_for('login'))
    return wrapper


def flash_error(msg):
    flash(msg)
    log.error(msg)


def calculate_pages(collection):
    # compute pagination
    if len(collection) == 0:
        pages = 0
    else:
        pages = len(collection) // cfg.pagesize
        if (len(collection) % cfg.pagesize) > 0:
            pages += 1
    return pages
