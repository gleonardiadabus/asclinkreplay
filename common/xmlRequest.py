from config import cfg

class XmlRequest:
    loginRequest="""
        <Marathon-Web-Player xmlform="login" language="en_EN" combi="0">
          <user>{USERNAME}</user>
          <password>{PASSWORD}</password>
          <combi-user/>
          <combi-password/>
        </Marathon-Web-Player>
    """

    logoutRequest="""
        <Marathon-Web-Player version="2.04.00" xmlform="logout" target="" language="en_EN" error="0"/>
    """

    mp3Request="""
        <Marathon-Web-Player xmlform="result" language="en_EN" action="play" wav="true">
          <call>
            <id>{CALLID_VALUE}</id>
          </call>
        </Marathon-Web-Player>
    """

    searchRequest="""
        <Marathon-Web-Player xmlform="search" language="en_EN">
          <from-date>
            <day>1</day>
            <month>1</month>
            <year>%s</year>
            <hour>0</hour>
            <minute>0</minute>
            <ampm>0</ampm>
          </from-date>
          <to-date>
            <day>1</day>
            <month>1</month>
            <year>%s</year>
            <hour>0</hour>
            <minute>0</minute>
            <ampm>0</ampm>
          </to-date>
          <utc>1</utc>
          <stereo />
          <channels><channel>*</channel></channels>
          <agents />
          <phone-numbers-intern />
          <phone-numbers-extern />
          <set-size>10</set-size>
          <ampm-format />
          <last-call-repeat>0</last-call-repeat>
          <fields>
              <field name='%s'>
                <value>{CONNID_VALUE}</value>
              </field>
          </fields>
          <duration/>
          <directions/>
          <sequences/>
          <phone-numbers-third/>
          <cdr-tags/>
        </Marathon-Web-Player>
    """ % (cfg.start_year,cfg.stop_year,cfg.connId)