# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module Conversation
#   FN20170606 creation
#   FN20170608 refactoring, serializer/deserializer
#   FN20170616 add page info
#   FN20170904 Serializing 'None' to 'N/D'
#   AI20171120 add last_played_at field


class ReplayConversation:
    def __init__(self, callId, connId, place=None, dnis=None,servizio=None,struttura=None,
                 esigenza=None,motivo_registrazione = None, starttime=None, endtime=None, durata=None, agentId=None,
                 thisDn=None,dnEsterno=None):
        self.callId=callId
        self.connId=connId
        self.place=place
        self.dnis=dnis
        self.servizio=servizio
        self.struttura=struttura
        self.esigenza=esigenza
        self.starttime=starttime
        self.endtime=endtime
        self.durata=durata
        self.agentId=agentId
        self.motivo_registrazione=motivo_registrazione
        self.thisDn=thisDn
        self.dnEsterno=dnEsterno
        self.url = None

    def serialize(self):
        return {
            '__type__': 'ReplayConversation',
            'connId': self.connId,
            'callId': self.callId,
            'place': self.place,
            'dnis': self.dnis,
            'servizio': self.servizio,
            'struttura': self.struttura,
            'esigenza': self.esigenza,
            'starttime': self.starttime,
            'endtime': self.endtime,
            'durata': self.durata,
            'agentId': self.agentId,
            'durata': self.durata,
            'motivo_registrazione' : self.motivo_registrazione,
            'thisDn': self.thisDn,
            'dnEsterno': self.dnEsterno,
            'url': self.url
        }
