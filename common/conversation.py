# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module Conversation
#   FN20170606 creation
#   FN20170608 refactoring, serializer/deserializer
#   FN20170616 add page info
#   FN20170904 Serializing 'None' to 'N/D'
#   AI20171120 add last_played_at field

from config import cfg, log
from datetime import datetime


class Conversation:
    def __init__(self, callid, servizio, esigenza, motivo_registrazione, telefono, outsourcer, direzione, durata, page, last_played_at):
        self.callid = callid
        self.servizio = servizio
        self.esigenza = esigenza
        self.telefono = telefono
        self.outsourcer = outsourcer
        self.direzione = direzione
        self.durata = durata
        self.motivo_registrazione = motivo_registrazione
        self.page = page
        self.last_played_at = last_played_at

    @staticmethod
    def value_for_field(parameters, field):
        for p in parameters:
            if p['name'] == field:
                return p['value']
        # error: field not found in parameters
        log.warn('Field %s is empty' % field)
        return ''

    def serialize(self):
        return {
            '__type__': 'Conversation',
            'callid': self.callid,
            'servizio': self.servizio,
            'esigenza': self.esigenza,
            'motivo_registrazione' : self.motivo_registrazione,
            'telefono': self.telefono,
            'outsourcer': self.outsourcer,
            'direzione': self.direzione,
            'durata': self.durata,
            'page': self.page,
            'last_played_at': self.last_played_at
        }

