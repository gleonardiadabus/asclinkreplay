# -*- coding: utf-8 -*-
#
# heraPlay - customized web player for customer Hera
#   version 0.25.2
#   date    2017-09-08
# Module CustomSerializer
#   FN20170609 creation
#   FN20160616 audit log serializer
#


from flask.json import JSONEncoder, JSONDecoder
from .conversation import Conversation


class CustomSerializer(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, Conversation):
            return obj.serialize()
        elif isinstance(obj, AuditLog):
            return obj.serialize()
        else:
            return super(CustomSerializer, self).default(obj)


class CustomDeserializer(JSONDecoder):
    def __init__(self, *args, **kwargs):
        JSONDecoder.__init__(self, object_hook=self.conversation_hook, *args, **kwargs)

    @staticmethod
    def conversation_hook(obj):
        if '__type__' not in obj:
            return obj
        elif obj['__type__'] == 'Conversation':
            return Conversation(obj['callid'], obj['servizio'], obj['esigenza'],obj['motivo_registrazione'], obj['telefono'], obj['outsourcer'],
                                obj['direzione'], obj['durata'], obj['page'], obj['last_played_at'])
        elif obj['__type__'] == 'AuditLog':
            return AuditLog(obj['date'], obj['subject'], obj['message'], obj['page'])
        return obj
